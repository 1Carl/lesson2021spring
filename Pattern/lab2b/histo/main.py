import numpy as np
import cv2 
from matplotlib import pyplot as plt
import os

def customnorm(image): 
    equ = cv2.equalizeHist(image)
    return equ

loc = os.path.join(os.curdir, "images/")

dim = (200, 200)

images = []
fixed = []
histo = []
fixhisto = []

for i in os.listdir(loc):
    tempimg = cv2.imread(os.path.join(loc, i))
    image = cv2.cvtColor(tempimg, cv2.COLOR_BGR2GRAY)
    images.append(cv2.resize(image, dim))
    fixed.append(np.zeros(dim))

for i in range(len(images)):
    histo.append(cv2.calcHist([images[i]],[0],None,[256],[0,256]))
    fixed[i] = customnorm(images[i])
    fixhisto.append(cv2.calcHist([fixed[i]],[0],None,[256],[0,256]))
    plt.subplot(4, 4, i * 4 + 1), plt.imshow(images[i], cmap = plt.cm.gray)
    plt.subplot(4, 4, i * 4 + 2), plt.plot(histo[i] )
    plt.subplot(4, 4, i * 4 + 3), plt.imshow(fixed[i], cmap = plt.cm.gray)
    plt.subplot(4, 4, i * 4 + 4), plt.plot(fixhisto[i])

plt.show()