import numpy as np
import cv2 
from matplotlib import pyplot as plt
import os
import skimage.morphology as morp
import skimage.filters.rank as rank

def answer(image):
    block = morp.disk(3)
    ans = rank.equalize(image, block)
    return ans

loc = os.path.join(os.curdir, "images/main.tif")

image = cv2.cvtColor(cv2.imread(loc), cv2.COLOR_BGR2GRAY)

gloEq = cv2.equalizeHist(image)

locEq = answer(image)

plt.subplot(1, 3, 1), plt.imshow(image, 'gray')
plt.subplot(1, 3, 2), plt.imshow(gloEq, 'gray')
plt.subplot(1, 3, 3), plt.imshow(locEq, 'gray')

plt.show()