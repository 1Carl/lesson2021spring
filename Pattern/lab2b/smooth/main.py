import numpy as np
import cv2 
from matplotlib import pyplot as plt
import os

loc = os.path.join(os.curdir, "images/img.tif")

image = cv2.cvtColor(cv2.imread(loc), cv2.COLOR_BGR2GRAY)
shows = []
coef = [3, 5, 9, 15, 35]

shows.append(image)

for i in range(len(coef)):
    block = np.full((coef[i], coef[i]), 1/(coef[i] * coef[i]))
    tmpimage = cv2.filter2D(image, -1, block)
    shows.append(tmpimage)

for i in range(len(shows)):
    plt.subplot(3, 2, i + 1), plt.imshow(shows[i], 'gray')

plt.show()