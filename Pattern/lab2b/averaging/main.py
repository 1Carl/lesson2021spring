import cv2
import os
import matplotlib.pyplot as plt
import numpy as np


img = cv2.cvtColor(cv2.imread('images/hub.tif'), cv2.COLOR_BGR2GRAY)
plt.subplot(1, 3, 1), plt.imshow(img, 'gray')


block = np.full((15, 15), 1/ (15 * 15))


avg = cv2.filter2D(img, -1, block)
plt.subplot(1, 3, 2), plt.imshow(avg, 'gray')


_, thr = cv2.threshold(avg, 65, 65, cv2.THRESH_BINARY)
plt.subplot(1, 3, 3), plt.imshow(thr, 'gray')


plt.show()