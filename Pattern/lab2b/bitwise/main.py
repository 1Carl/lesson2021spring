import numpy as np
import cv2 
from matplotlib import pyplot as plt
from matplotlib import image as mpimg
import glob
import math
import os

loc = os.path.join(os.curdir, "images/")

dim = (512, 512)

image = cv2.resize(cv2.cvtColor(cv2.imread(os.path.join(loc, "im.png")), cv2.COLOR_BGR2GRAY), dim)

one = cv2.resize(cv2.cvtColor(cv2.imread(os.path.join(loc, "one.png")), cv2.COLOR_BGR2GRAY), dim)
two = cv2.resize(cv2.cvtColor(cv2.imread(os.path.join(loc, "two.png")), cv2.COLOR_BGR2GRAY), dim)

plt.subplot(2, 3, 1), plt.imshow(image, 'gray')
plt.subplot(2, 3, 2), plt.imshow(one, 'gray')
plt.subplot(2, 3, 3), plt.imshow(cv2.bitwise_and(image, one), 'gray')
plt.subplot(2, 3, 4), plt.imshow(image, 'gray')
plt.subplot(2, 3, 5), plt.imshow(two, 'gray')
plt.subplot(2, 3, 6), plt.imshow(cv2.bitwise_or(image, two), 'gray')

plt.show()