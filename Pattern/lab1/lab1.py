import cv2
import os
import numpy as np

images = []
dim = (200, 200)
loc = os.path.join(os.curdir, "images/")
for i in os.listdir(loc):
    tempimg = cv2.imread(os.path.join(loc, i))
    image = cv2.cvtColor(tempimg, cv2.COLOR_BGR2GRAY)
    images.append(cv2.resize(image, dim))

avg = np.zeros(dim, dtype=np.float64)

for i in images:
    avg += i / len(images)

for i in images:
    avg -= i

cv2.imshow("image", avg.astype(np.uint8))
cv2.waitKey(0)
cv2.imwrite(os.path.join(loc, "../average.jpeg"), avg.astype(np.uint8))
