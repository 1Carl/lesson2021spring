from PIL import Image, ImageOps
import sys
import glob

# Trim all png images with white background in a folder
# Usage "python PNGWhiteTrim.py ../someFolder"

filePaths = glob.glob("/home/carl/lesson2021spring/Pattern/coin/image/*.jpg") #search for all png images in the folder

for filePath in filePaths:
    print("sie")
    image=Image.open(filePath)
    image.load()
    imageSize = image.size
    
    # remove alpha channel
    invert_im = image.convert("RGB") 
    
    # invert image (so that white is 0)
    invert_im = ImageOps.invert(invert_im)
    imageBox = invert_im.getbbox()
    
    cropped=image.crop(imageBox)
    cropped.save(filePath)