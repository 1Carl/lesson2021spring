import cv2
import os
import numpy as np
import matplotlib as plt

loc = os.path.join(os.curdir, "images/")
image = cv2.cvtColor(cv2.imread(os.path.join(loc, "firefox.jpeg")), cv2.COLOR_BGR2GRAY) 
cv2.waitKey(0)

lv0 = (128, 128)
lv1 = (64, 64)
lv2 = (32, 32)
dim = (1024, 1024)

images = []

images.append(cv2.resize(image, lv0))
images.append(cv2.resize(image, lv1))
images.append(cv2.resize(image, lv2))

# board = 

for j in range(5):
    for i in range(len(images)): 
        try:
            cv2.imwrite(os.path.join(loc, "../result/some"  + str(j)+ str(i) +  ".jpg"), cv2.resize(images[i], dim, interpolation=j))
        except:
            print("${j} is not defined")


# for i in range(len(images)): 
#     cv2.imwrite(os.path.join(loc, "../result/bilinear" + str(i) + ".jpg"), cv2.resize(images[i], dim, interpolation=cv2.INTER_LINEAR))