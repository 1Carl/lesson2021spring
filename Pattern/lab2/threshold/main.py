import cv2 as c
import os
import numpy as np
import matplotlib.pyplot as plt

loc = os.path.join(os.curdir, "images/")
image = c.cvtColor(c.imread(os.path.join(loc, "cat.jpg")), c.COLOR_RGB2GRAY)

divider = 2
# dim = image.shape
# tmp = np.zeros(dim, float)
board = plt.figure(figsize=(10, 5))
board.figsize = (1024, 1024)

tmpimage = np.copy(image)

for i in range(8):
    res = board.add_subplot(2, 4, i + 1)
    res.imshow(tmpimage * 2 ** i, cmap = plt.cm.gray)
    tmpimage = tmpimage // divider


plt.show()

# not necessary
# ret, tmp = c.threshold(image, 127, 255, c.THRESH_BINARY)
# c.imwrite(os.path.join(loc, "../result/uint" + str(8) + ".jpeg"), tmp)