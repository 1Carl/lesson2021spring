import cv2
import os
import numpy as np
import matplotlib.pyplot as plt

loc = os.path.join(os.curdir, "images/")
image = cv2.cvtColor(cv2.imread(os.path.join(loc, "image.png")), cv2.COLOR_BGR2GRAY)

board = plt.figure(figsize=(10, 5))
board.figsize = (1024, 1024)

for i in range(1, 9, 1): 
    tmpimage = np.copy(image)
    with np.nditer(tmpimage, op_flags=['readwrite']) as ar:
        for x in ar:
            if x >= (2 ** i):
                x[...] = x % (2 ** i)

    res = board.add_subplot(2, 4, i)
    # res.imshow(tmpimage.astype(np.uint8))
    res.imshow(tmpimage, cmap = plt.cm.gray)
    
plt.show()