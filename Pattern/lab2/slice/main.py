import cv2
import os
import numpy as np
import matplotlib.pyplot as plt

fmin = 100
fmax = 180
smin = 75
smax = 125

loc = os.path.join(os.curdir, "images/")
image = cv2.cvtColor(cv2.imread(os.path.join(loc, "input.png")), cv2.COLOR_BGR2GRAY)

im1 = np.copy(image)

with np.nditer(im1, op_flags=['readwrite']) as ar:
    for x in ar:
        if x < fmax and x > fmin:
            x[...] = 0
        else:
            x[...] = 230

# print(im1)

im2 = np.copy(image)

with np.nditer(im2, op_flags=['readwrite']) as ar:
    for x in ar:
        if x < smax and x > smin:
            x[...] = 230
        else:
            x[...] = x / 2


# print(im2)


images = [image, im1, im2]
titles = ["original", "slice1", "slice2"]
for i in range(3):
    plt.subplot(1, 3,i+1),plt.imshow(images[i],'gray',vmin=0,vmax=255)
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])

plt.show()