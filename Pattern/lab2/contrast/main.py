import cv2
import os
import numpy as np
import matplotlib.pyplot as plt

loc = os.path.join(os.curdir, "images/")
image = cv2.cvtColor(cv2.imread(os.path.join(loc, "bean.png")), cv2.COLOR_BGR2GRAY)

board = plt.figure(figsize=(10, 5))
board.figsize = (1024, 1024)

main = board.add_subplot(1, 3, 1)
main.imshow(cv2.imread(os.path.join(loc, "bean.png")))

dim = image.shape

maxx = np.full(dim, image.max())
minn = np.full(dim, image.min())

img = ((image - minn) / (maxx - minn) * 255).astype(np.uint8)

im = board.add_subplot(1, 3, 2)
im.imshow(img, cmap = plt.cm.gray)


nex =(np.round(img / 255) * 255).astype(np.uint8)

mnex = board.add_subplot(1, 3, 3)
mnex.imshow(nex, cmap = plt.cm.gray)

plt.show()

