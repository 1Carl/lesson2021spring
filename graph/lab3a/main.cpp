#include <GL/freeglut.h>
#include <math.h>

 

struct point {
int x;
int y;
};

 

void render(void);
void drawTween(point A, point B, int n, float t);
void timer(int value);

 

//////////////////////////////
GLfloat delT = 0.1;
GLfloat t = 0.0;

 

int n = 8;

 

 

int main(int argc, char** argv) {
glutInit(&argc, argv);
glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
glutInitWindowPosition(100, 100);
glutInitWindowSize(400, 400);
glutCreateWindow("Tweeining animation");
glutTimerFunc(33, timer, 1);
glutDisplayFunc(render);
glutMainLoop();
}

 

void drawTween(point a[], point b[], int n, float t) {
int i = 0;
glBegin(GL_LINE_LOOP);
for (i = 0; i < 8; i++) {
glVertex2f((1 - t)*a[i].x + b[i].x*t, (1 - t)*a[i].y + b[i].y*t);
}
glEnd();
}

 

void timer(int value) {

//////////////////////////////
t += delT;
if (t >= 1.0 || t <= 0.0) delT = -delT;

glutPostRedisplay();
glutTimerFunc(60, timer, 1);
};

 

void render(void) {
// glClear(GL_COLOR_BUFFER_BIT);

glMatrixMode(GL_PROJECTION);
glLoadIdentity();

 

glOrtho(0, 7, 0, 7, 1.0f, -1.0f);

int i;
point a[8] = { {2,5},{5,5},{5,4},{4, 4},{4, 3},{3,3},{3,4},{2,4} };
point b[8] = { {1,6},{6,6},{6,4},{6, 2},{3, 2},{2,2},{1,2},{1,3} };

 

// glBegin(GL_LINE_LOOP);
// for(i=0; i<n; i++){
// glVertex2f(a[i].x, a[i].y);
// }
// glEnd();

 


////////////////////////
glClear(GL_COLOR_BUFFER_BIT);
drawTween(a, b, n, t);
glutSwapBuffers();

 


}
