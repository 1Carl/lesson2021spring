#include <GL/glut.h>
#include <iostream>
#include <cmath>
#include <cstdlib>

using namespace std;

int side = 6;

float winHeight = 50, winWidth = 50;

int posx = 0, posy = 0;

void render(void);

void keyboard(unsigned char c, int x, int y);

void special(int key, int x, int y);

void mouse(int button, int state, int x, int y);

void resize(GLsizei wh, GLsizei ww);

void Displaysinex(void);

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(500, 400);
    glutCreateWindow("Interactive polygon");
    glutDisplayFunc(Displaysinex);
    // glutDisplayFunc(render);
    glutReshapeFunc(resize);
    glutMainLoop();
}

void render(void)
{
    GLfloat ini = 0.05;
    float radius = 0.08;
    for (int j = 0; j < 50; j++)
    {
        glBegin(GL_LINE_STRIP);
            glColor3f(1, 0.5, 0.5);
            for (int i = 0; i < 7; ++i)
            {
                glColor3f(1, 0.5, 0.5);
                glVertex2d((radius * sin(i / 6.0 * 2 * M_PI + ini)), (radius * cos(i / 6.0 * 2 * M_PI + ini)));
            }
        glEnd();
        ini += 0.1;
        radius += 0.03;
    }
    glBegin(GL_LINE_STRIP);
        for (int i = 0; i < 5; ++i)
        {
            glColor3f(1, 0.5, 0.5);
            glVertex2d((0.7 * sin(i / 4.0 * 2 * M_PI + M_PI / 4)), (0.7 * cos(i / 4.0 * 2 * M_PI + M_PI / 4)));
        }
    glEnd();
}

void Displaysinex(void)
{
    GLfloat degtorads, x;
    int i;
    degtorads = M_PI / 180.0;
    glClearColor(1, 1, 1, 1);
    for (int j = 0; j < 4; j++)
    {
        for (int i = 0; i < 5; i++)
        {   
            glViewport(posx, posy, winWidth, winHeight);
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glScalef((i + j) % 2 == 0 ? 1.0:-1.0, 1.0, 0.0);
            render();
            posx = i * winWidth;
        }
        posy = j * winHeight;
    }
    glutSwapBuffers();
}

void resize (GLsizei w, GLsizei h){
    winWidth = w / 5;
    winHeight = h / 4;
    Displaysinex();
}