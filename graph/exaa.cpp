#include <GL/glut.h>
#include <math.h>

// void drawline(float from_x, float from_y, float to_x, float to_y)
// {
//     // From coordinate position
//     glVertex2f(from_x, from_y);

//     // To coordinate position
//     glVertex2f(to_x, to_y);
// }

// void drawShape()
// {
//     glColor3f(1.0, 1.0, 0.0); // Color (RGB): Yellow
//     glLineWidth(2.0); // Set line width to 2.0

//     // Draw line
//     glBegin(GL_LINES);
//     drawline(0.25, 0.5, 0.4, 0.5);
//     drawline(0.4, 0.6, 0.4, 0.5);
//     drawline(0.4, 0.4, 0.4, 0.5);
//     drawline(0.6, 0.5, 0.75, 0.5);
//     glEnd();

//     // Draw triangle
//     glBegin(GL_TRIANGLES);
//     glVertex2f(0.4, 0.5);
//     glVertex2f(0.6, 0.6);
//     glVertex2f(0.6, 0.4);
//     glEnd();
// }

void render(void)
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho( -2.0f, 2.0f, -2.0f, 2.0f, 2.0f, 2.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Draw shape one
    glPushMatrix();
        glTranslatef(-0.5, -0.5, 0.0);
        glColor3f(0.4f, 0.4f, 1.0f);
        glBegin(GL_POLYGON);
            glVertex2f(-0.3f, 0.3f);
            glVertex2f(-0.3f, -0.3f);
            glVertex2f(0.3f, -0.3f);
            glVertex2f(0.3f, 0.3f);
        glEnd();
    glPopMatrix();

    glPushMatrix();
        glTranslatef(0.5, 0.5, 0.0);
        glColor3f(0.7f, 0.2f, 0.2f);
        glBegin(GL_POLYGON);
            for (int i = 0; i < 360; i++)
            {
                GLfloat x = 0.2f * cos((float) i * M_PI / 180);
                GLfloat y = 0.2f * sin((float) i * M_PI / 180);
                glVertex2f(x, y);
            }
        glEnd();
    glPopMatrix();
    // glBegin(GL_POLYGON);
    //     glColor3f(0.0f, 0.0f, 1.0f);
    //     glVertex2f(-0.3f, 0.3f);
    //     glVertex2f(-0.3f, -0.3f);
    //     glVertex2f(0.3f, -0.3f);
    //     glVertex2f(0.3f, 0.3f);
    // glEnd();
    glutSwapBuffers();
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(500, 500);
    glutCreateWindow("My OpenGL program");
    glutDisplayFunc(render);
    glutMainLoop();
    return 0;
}