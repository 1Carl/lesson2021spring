#include <GL/freeglut.h>
#include <math.h>

struct point
{
    float x;
    float y;
};

void render(void);
void drawTween(point A, point B, int n, float t);
void timer(int value);

GLfloat delT = 0.1;
GLfloat t = 0.0;

int n = 8;

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(400, 400);
    glutCreateWindow("Tweeining animation");
    glutTimerFunc(33, timer, 1);
    glutDisplayFunc(render);
    glutMainLoop();
}

void drawTween(point a[], point b[], int n, float t)
{
    int i = 0;
    glBegin(GL_LINES);
    for (i = 0; i < 20; i++)
    {
        glVertex2f((1 - t) * a[i].x + b[i].x * t, (1 - t) * a[i].y + b[i].y * t);
    }
    glEnd();
    glBegin(GL_LINE_STRIP);
    for (int i = 0; i < 360; i++)
    {
        int num = (((float)i / 360) * 360);
        while (num > 360.0)
        {
            num -= 360;
        }

        while (num < 0)
        {
            num += 360;
        }
        glColor3f(1.0f, 1.0f, 1.0f);
        GLfloat x = 0.8 * cos((float)i * M_PI / 360 * 2) + 4;
        GLfloat y = 1 * sin((float)i * M_PI / 360 * 2) + 6;
        glVertex3f(x, y, 0.0f);
    }
    glEnd();
    glBegin(GL_LINES);

    glEnd();
}

void timer(int value)
{
    t += delT;
    if (t >= 1.0 || t <= 0.0)
        delT = -delT;

    glutPostRedisplay();
    glutTimerFunc(60, timer, 1);
};

void render(void)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, 8, 0, 8, 1.0f, -1.0f);

    int i;
    point a[20] = {
        {4, 5}, {4, 3}, //main
        {4, 3}, {4, 2}, //hul - deed
        {4, 2}, {4, 1}, //hul - dood
        {4, 3}, {4, 2}, //hul - deed
        {4, 2}, {4, 1}, //hul - dood
        {4, 5}, {4, 4}, //gar - deed
        {4, 4}, {4, 3}, //gar - dood
        {4, 5}, {4, 4}, //gar - deed
        {4, 4}, {4, 3}, //gar - dood
        };
    point b[20] = {
        {4, 5}, {4, 3}, //main
        {4, 3}, {5, 2}, //hul - deed
        {5, 2}, {5, 1}, //hul - dood
        {4, 3}, {3, 2}, //hul - deed
        {3, 2}, {3, 1}, //hul - dood
        {4, 5}, {5, 4}, //gar - deed
        {5, 4}, {6, 5}, //gar - dood
        {4, 5}, {3, 4}, //gar - deed
        {3, 4}, {2, 5}, //gar - dood
        };

    glClear(GL_COLOR_BUFFER_BIT);
    drawTween(a, b, n, t);
    glutSwapBuffers();
}