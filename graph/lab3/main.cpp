#include <GL/glut.h>
#include <iostream>
#include <cmath>
#include <cstdlib>
#define radius 0.5

using namespace std;

void render(void);

void keyboard(unsigned char c, int x, int y);

void special(int key, int x, int y);

void mouse(int button, int state, int x, int y);


int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(300, 300);
    glutCreateWindow("Circle");

    glutDisplayFunc(render);
    // glutKeyboardFunc(keyboard);
    glutSpecialFunc(special);
    glutMouseFunc(mouse);

    glutMainLoop();

}


void keyboard(unsigned char c, int x, int y) {
    if (c == 27) {
        exit(0);
    }
}

 
void mouse(int button, int state, int x, int y) {
    if (button == GLUT_RIGHT_BUTTON) {
        exit(0);
    }
}



 
void render(void){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1, 1, 1, 1);
    glBegin(GL_TRIANGLE_FAN);
        for (int i = 0; i < 1000; i++)
        {
            glColor3f( (float) i / 1000, abs((float)  i / 1000 - 1), i < 0.5? i + 0.5:i-0.5);
            GLfloat x = radius * cos((float) i * M_PI / 500);
            GLfloat y = radius * sin((float) i * M_PI / 500);
            glVertex3f(x, y, 0.0f);
        }
    glEnd();

    glutSwapBuffers();
}