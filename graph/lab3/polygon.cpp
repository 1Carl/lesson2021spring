#include <GL/glut.h>
#include <iostream>
#include <cmath>
#include <cstdlib>
#define radius 0.8

using namespace std;

int side = 3;
float k = 0;

void render(void);

void keyboard(unsigned char c, int x, int y);

void special(int key, int x, int y);

void mouse(int button, int state, int x, int y);

struct CustomColor {
    GLfloat red, green, blue;
}a, *p = &a;

void HSVtoRGB(float H, float S,float V){
    if(H>360 || H<0 || S>100 || S<0 || V>100 || V<0){
        cout<<"The givem HSV values are not in valid range"<<endl;
        return;
    }
    float s = S/100;
    float v = V/100;
    float C = s*v;
    float X = C*(1-abs(fmod(H/60.0, 2)-1));
    float m = v-C;
    float r,g,b;
    if(H >= 0 && H < 60){
        r = C,g = X,b = 0;
    }
    else if(H >= 60 && H < 120){
        r = X,g = C,b = 0;
    }
    else if(H >= 120 && H < 180){
        r = 0,g = C,b = X;
    }
    else if(H >= 180 && H < 240){
        r = 0,g = X,b = C;
    }
    else if(H >= 240 && H < 300){
        r = X,g = 0,b = C;
    }
    else{
        r = C,g = 0,b = X;
    }
    p->red = r + m;
    p->blue = b + m;
    p->green = g + m;
}


int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(300, 300);
    glutCreateWindow("Interactive polygon");

    glutDisplayFunc(render);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special);
    glutMouseFunc(mouse);

    glutMainLoop();
}

void keyboard(unsigned char c, int x, int y) {
    if (c == 27) {
        exit(0);
    }

    if (c == 97) {
        k+= 1 * M_PI / 180;
    }
    if (c == 100) {
        k-= 1 * M_PI / 180;
    }
    render();
}

void special(int key, int x, int y){

    if (key == GLUT_KEY_DOWN || key == GLUT_KEY_LEFT) {
        side > 3? side--:side;
    }
    if (key == GLUT_KEY_UP || GLUT_KEY_RIGHT) {
        side++;
    }
    render();
}

void mouse(int button, int state, int x, int y) {
    if (button == GLUT_RIGHT_BUTTON) {
        exit(0);
    }
}

 
void render(void){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1, 1, 1, 1);
    glBegin(GL_TRIANGLE_FAN);
        for (int i = 0; i < side; i++)
        {
            int num = (((float) i / side) * 360 - k * 180 / M_PI);
            while (num > 360.0){
                num -= 360;
            }

            while (num < 0)
            {
                num+=360;
            }
            
            HSVtoRGB(num, 1.0 * 100, 1.0 * 100);
            glColor3f(a.red, a.green, a.blue);
            GLfloat x = radius * cos((float) i * M_PI / side * 2 + k);
            GLfloat y = radius * sin((float) i * M_PI / side * 2 + k);
            glVertex3f(x, y, 0.0f);
        }
    glEnd();

    glutSwapBuffers();
}