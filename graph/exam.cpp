#include<iostream>
#include <GL/freeglut.h>

void setup(){
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    
}

void display(){
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1.0f, 0.0f, 0.0f);
    glRectf(x1, y1, x1+rsize, y1+rsize);
    glutSwapBuffers();
}


using namespace std;

int main() {
    glutInitDisplayMode(GL_DOUBLE | GL_RGB);
    glutCreateWindow("Tween exam");
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glutTimerFunc(33, timer, 1);
    setup();
    glutMainLoop();
}

