-- 2. Сурагчдын ангийн мэдээллийн зохицуулдаг 2 триггер бич. Хэрвээ шинээр нэмэгдсэн мөрийн анги нь 9-с бага эсвэл 12 -с их байвал ангийг NULL утгатай болгох. Хэрвээ нэмэгдэж орсон мөрийн анги нь NULL байвал түүнийг 9 болох.

CREATE TRIGGER trg_grader1 ON Highschooler
    FOR INSERT 
    AS
BEGIN
    UPDATE Highschooler
    SET grade = NULL
    FROM Highschooler h
    RIGHT JOIN inserted i ON i.ID = h.ID
    WHERE (i.grade < 9 OR i.grade > 12) AND i.ID IS NOT NULL
END;



INSERT into Highschooler
VALUES(1543, 'Angeline', 7);

SELECT * FROM Highschooler


