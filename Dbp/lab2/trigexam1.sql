
-- 1. 'Friendly' нэртэй шинэ сурагч нэмэгдвэл түүнийг ангийнхаа бүх сурагчдад дуртай болгох.



CREATE TRIGGER friendly ON Highschooler
    FOR INSERT 
    AS
BEGIN
    INSERT INTO Likes
    SELECT i.ID, h.ID FROM inserted i, Highschooler h
    WHERE i.ID <> h.ID AND i.grade = h.grade AND i.name = 'Friendly'
END;



INSERT into Highschooler
VALUES(1232, 'Friendly', 9);

SELECT * FROM Likes