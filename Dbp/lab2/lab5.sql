
-- 1. 'Friendly' нэртэй шинэ сурагч нэмэгдвэл түүнийг ангийнхаа бүх сурагчдад дуртай болгох.

CREATE TRIGGER friendly ON Highschooler
    FOR INSERT 
    AS
BEGIN
    IF (SELECT COUNT(*) FROM inserted WHERE name = 'Friendly') > 0
    BEGIN 
        WITH FriendlyClassMates AS (
            SELECT ID FROM Highschooler
            WHERE grade = (
                SELECT grade FROM Highschooler
                WHERE name = 'Friendly'
            )
        )
        INSERT INTO Likes 
        SELECT b.ID, f.ID FROM FriendlyClassMates f, (
            SELECT ID FROM Highschooler
            WHERE name = 'Friendly'
        ) as b
    END
END;

INSERT into Highschooler
VALUES(1232, 'Friendly', 9);


-- 4. Найзын харьцааны тэгш хэмтэй байдлыг хадгалах триггерүүдийг бичнэ үү. Жишээлбэл, хэрвээ (A, B) Friend-с хасагдвал (B, A) ч мөн хасагдах, шинээр нэмэгдэх үед мөн адил. Найзын харьцаанд Update хийгдэхгүй гэж үзнэ.


