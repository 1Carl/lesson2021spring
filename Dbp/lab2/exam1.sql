-- -- --a. 1990-с хойш зохиогдсон бүх киноны mID-г харуул (5 оноо)


-- SELECT mID FROM Movie
-- WHERE year > 1990;


-- -- --b. Хоёроос дээш удаа үнэлгээ (Rating) өгсөн reviewer-үүдийг харуулна уу. (10 оноо)


-- SELECT r.* FROM Reviewer r
-- LEFT JOIN (
--     SELECT COUNT(*) AS too, rID FROM Rating GROUP BY rID
-- ) c on c.rID = r.rID
-- WHERE c.too > 2;


-- -- --с. Ядаж нэг удаа 5 үнэлгээ авсан киног харуулна уу. (10 оноо)


-- SELECT m.* FROM Movie m
-- LEFT JOIN (
--     SELECT mID, MAX(stars) _max FROM Rating 
--     GROUP BY mID
-- ) a on m.mID = a.mID
-- WHERE a._max = 5;


-- -- -- d. Хэрвээ titanic нэртэй кино шинээр нэмэгдвэл түүнд бүх reviewer-г 5 үнэлгээ өгдөг триггер бич (10 оноо),


-- CREATE TRIGGER bestMovie ON Movie 
--     FOR INSERT
--     AS
-- BEGIN
--     INSERT INTO Rating
--     SELECT r.rID, i.mID, 5, CONVERT(VARCHAR(10), GETDATE(), 20) 
--     FROM Reviewer r, inserted i
--     WHERE i.title = 'Titanic'
-- END


-- INSERT INTO Movie
-- VALUES(110, 'Titanic', 1997, 'James Cameron')



-- --e. Movie хүснэгтэд шинэ кино нэмэгдээд, тэр киноны mID давхацвал шинэ давхцахгүй ID оноож, хүснэгтэд нэмэх триггер бич. (10 оноо),


CREATE TRIGGER idGenerator ON Movie 
    INSTEAD OF INSERT
    AS
BEGIN
    INSERT INTO Movie
    SELECT  (
        CASE WHEN mID in (
            SELECT mID FROM Movie
        ) THEN (
            SELECT MAX(mID) + 1 FROM Movie
        ) ELSE mID END
    ) ,title, year, director FROM inserted
END

SELECT * FROM Movie


INSERT INTO Movie
VALUES(101, 'Mugen Train', 2021, 'Haruo Sotozaki')


