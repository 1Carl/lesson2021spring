-- 3. Сурагчдын анги 12-с их болж өөрчлөгдөхөд HIghschooler-с устгаж Highschooler_history хүснэгтэд нэмэх. Хэрвээ нэг сурагч анги дэвшвэл түүний найзуудыг ч мөн дэвшүүлэх.


CREATE TRIGGER trg_archiver ON Highschooler
    FOR UPDATE
    AS
BEGIN
    INSERT INTO Highschooler_history
    SELECT d.* FROM deleted d, inserted i
    WHERE d.ID = i.ID AND i.grade > 12
    DELETE FROM Highschooler
    WHERE grade > 12
END

SELECT * FROM Highschooler

UPDATE Highschooler 
SET grade = grade + 1
WHERE id = 1661

SELECT * FROM Highschooler_history


