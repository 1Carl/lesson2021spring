

CREATE TRIGGER trg_grader2 ON Highschooler
    FOR INSERT 
    AS
BEGIN
    UPDATE Highschooler
    SET grade = 9
    FROM Highschooler h
    RIGHT JOIN inserted i ON i.ID = h.ID
    WHERE i.grade IS NULL AND i.ID IS NOT NULL
END;


INSERT into Highschooler
VALUES(1234, 'Badi', NULL);

SELECT * FROM Highschooler

