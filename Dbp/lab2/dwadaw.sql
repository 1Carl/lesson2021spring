-- 5. 

-- 6. Сурагч A болон B нь найзууд бөгөөд A нь B-д дуртай гэхдээ B нь A-д дуртай мэдээлэл байхгүй бол энэ 2 сурагчийн талаарх мэдээллийг Likes хүснэгтээс устгая.


-- 7. Сурагч А нь В-тэй найз, В нь С-тэй найз бол А,С-г найзууд болгож, 
--Friend хүснэгтэд мэээлэл нэмнэ үү (A,C нь найзууд бол дахин нэмэх шаардлагагүй, мөн сурагч өөртэйгээ найз болохгүй).




WITH Liked as (
    SELECT * FROM Likes
    INTERSECT
    SELECT * FROM Friend
    EXCEPT
    SELECT l1.* FROM Likes l1, Likes l2
    WHERE l1.ID1 = l2.ID2 AND l2.ID1 = l1.ID2
)

DELETE FROM Likes 
WHERE EXISTS (
    SELECT * FROM Liked 
    WHERE Likes.ID1 = Liked.ID1 AND Liked.ID2 = Likes.ID2
)



WITH ftadds AS (
    SELECT f1.ID1, f2.ID2 FROM Friend f1, Friend f2
    WHERE f1.ID2 = f2.ID1 AND f2.ID2 < f1.ID1 
)

INSERT INTO Friend
SELECT * FROM ftadds
WHERE NOT EXISTS (
    SELECT * FROM Friend 
    WHERE Friend.ID1 = ftadds.ID1 AND Friend.ID2 = ftadds.ID2
)

