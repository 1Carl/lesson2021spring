--1

select r.name, m.title, ra.stars, ra.ratingDate
from Reviewer r, Movie m, Rating ra
where r.rID = ra.rID and m.mID = ra.mID
ORDER by r.name, m.title, ra.stars desc
    
--2 

select distinct m.title, max(r.stars) over (PARTITION by r.mID) as highest_rate from Movie m, Rating r
WHERE m.mID = r.mID
Order by m.title

--3

SELECT distinct m.title, (MAX(r.stars) OVER (PARTITION by r.mID) - MIN(r.stars) OVER (Partition by r.mID)) as tarhalt
FROM Movie m, Rating as r
WHERE m.mID = r.mID
ORDER by m.title, tarhalt DESC


SELECT (AVG(case when t.YEAR < 1980 then t._avg end) - AVG(case when t.YEAR >=1980 then t._avg end)) as zoruu
from  (select distinct r.mID, m.YEAR, AVG(r.stars)  OVER (PARTITION by r.mID) as _avg 
        from Rating r, Movie m
        WHERE r.mID = m.mID) as t
