-- 4. Найзын харьцааны тэгш хэмтэй байдлыг хадгалах триггерүүдийг бичнэ үү. 
-- Жишээлбэл, хэрвээ (A, B) Friend-с хасагдвал (B, A) ч мөн хасагдах, шинээр нэмэгдэх үед мөн адил. 
-- Найзын харьцаанд Update хийгдэхгүй гэж үзнэ.


CREATE TRIGGER trg_exam4a ON Friend
    AFTER INSERT
    AS
BEGIN
    INSERT INTO FRIEND
    SELECT ID2, ID1 FROM inserted i
    WHERE NOT EXISTS (
        SELECT * FROM Friend
        WHERE ID1 = i.ID2 AND ID2 = i.ID1
    )
END



select * from Friend
order by ID1;


INSERT INTO Friend 
VALUES(1911, 1782)
