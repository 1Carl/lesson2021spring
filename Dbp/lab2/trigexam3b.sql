-- 3. Сурагчдын анги 12-с их болж өөрчлөгдөхөд HIghschooler-с устгаж Highschooler_history хүснэгтэд нэмэх. Хэрвээ нэг сурагч анги дэвшвэл түүний найзуудыг ч мөн дэвшүүлэх.
CREATE TRIGGER trg_updater ON Highschooler
    FOR UPDATE
    AS
BEGIN
    IF CONTEXT_INFO() <> 0x5555
    BEGIN
        SET Context_Info 0x5555;
        WITH updation AS (
            SELECT i.ID
            FROM deleted d, inserted i
            WHERE i.ID = d.ID AND d.grade + 1 = i.grade
        )
        UPDATE Highschooler
        SET grade += 1
        FROM Highschooler h
        RIGHT JOIN (
            SELECT ID1 FROM Friend 
            WHERE ID2 IN (
                SELECT ID FROM updation
            )
        ) u ON u.ID1 = h.ID
        SET Context_Info 0x1234;
    END
END


SELECT * FROM Highschooler




UPDATE Highschooler 
SET grade = grade + 1
WHERE id = 1101


SELECT * FROM Highschooler_history

SELECT * FROM Friend
ORDER BY ID1