-- 1. Gabriel нэртэй сурагчтай найзын харицаатай бүх оюутнуудын нэрийг харуулна уу.
SELECT h2.name FROM Friend f 
LEFT JOIN Highschooler h1 ON f.ID1 = h1.ID
LEFT JOIN Highschooler h2 ON f.ID2 = h2.ID
WHERE h1.name = 'Gabriel'


-- 2. Өөрөөсөө 2-оос олон дүү хэн нэгэнд дуртай бүх сурагчдын нэр ангийг болон дуртай  сурагчийнх нь нэр ангийг харуул.
SELECT h1.name, h1.grade, h2.name, h2.grade FROM Likes l 
LEFT JOIN Highschooler h1 ON l.ID1 = h1.ID
LEFT JOIN Highschooler h2 ON l.ID2 = h2.ID
WHERE h1.grade - 2 >= h2.grade


-- 3. Өөр хоорондоо дуртай сурагчдийн хосуудын нэр ангиудыг харуул. Нэг хос давхцаж болохгүй ба нэрсийг цагаан толгойн дарааллаар харуулна уу.

WITH LikeEachOther AS (
    SELECT h1.name, h1.grade, h2.name AS aname, h2.grade AS agrade 
    FROM (
        SELECT l1.ID1, l1.ID2 
        FROM Likes l1, Likes l2 
        WHERE l1.ID1 > l2.ID1 AND l1.ID1 = l2.ID2 AND l2.ID1 = l1.ID2
    ) t
    LEFT JOIN Highschooler h1 ON h1.ID = t.ID1
    LEFT JOIN Highschooler h2 ON h2.ID = t.ID2
)

SELECT name, grade, aname, agrade FROM LikeEachOther
ORDER BY name


-- 4. Likes хүснэгтэд байхгүй бүх сурагчдын нэрсийг ангитай нь олж, анги, нэрээр эрэмбэлж харуулна уу.


WITH Liked AS (
    SELECT ID1 FROM Likes 
    UNION 
    SELECT ID2 FROM Likes
)

SELECT grade, name 
FROM Highschooler
LEFT JOIN Liked ON Highschooler.ID = Liked.ID1
WHERE Liked.ID1 IS NULL
ORDER BY grade, name


-- 5. Сурагч А нь сурагч Б-д дуртай бөгөөд сурагч Б хэнд дуртай талаар мэдээлэл байхгүй 
-- (Б Likes хүснэгтэд id1 -р бичэгдээгүй) байх бүх тохиолдолыг олж, А болон Б-ийн нэр, ангийг нэрээр эрэмбэлж харуулна уу.


SELECT h1.name, h1.grade, h2.name, h2.grade 
FROM Likes
LEFT JOIN Highschooler h1 ON h1.ID = Likes.ID1
LEFT JOIN Highschooler h2 ON h2.ID = Likes.ID2
WHERE ID2 NOT IN (
    SELECT ID1 FROM Likes
)
ORDER by h1.name, h2.name

-- 6. Зөвхөн нэг ижил ангид л найзууд нь байдаг сурагчдын нэр ангийг олж, анги нэрээр эрэмбэлж харуулна уу.


WITH Fs(ID1, ID2, name, grade, aname, agrade) AS (
    SELECT Friend.*, h1.name, h1.grade, h2.name, h2.grade FROM Friend 
    LEFT JOIN Highschooler h1 ON h1.ID = ID1
    LEFT JOIN Highschooler h2 ON h2.ID = ID2
)

SELECT DISTINCT ID1,grade,name FROM Fs
WHERE ID1 NOT IN (
    SELECT ID1 FROM Fs 
    WHERE fs.grade <> fs.agrade
) AND EXISTS ( 
    SELECT  ID1 FROM Fs 
    WHERE fs.grade = fs.agrade
)
ORDER by grade, name

-- 7. А, В сурагчид өөр хоорондоо найз биш хэдийч А сурагч нь В сурагчид дуртай бөгөөд тэд 2-уул С сурагчтай найз бол А,В,С гурвалын нэр ангийг харуул.

SELECT h1.*, h2.*, h3.* FROM (
    SELECT * FROM Likes 
    EXCEPT 
    SELECT * FROM Friend
) l, Friend f1, Friend f2, Highschooler h1, Highschooler h2, Highschooler h3
WHERE l.ID1 = f1.ID1 AND 
l.ID2 = f2.ID1 AND 
f1.ID2 = f2.ID2 AND 
h1.ID = l.ID1 AND 
h2.ID = l.ID2 AND 
h3.ID = f1.ID2 

-- 8. Уг сургуулийн хүүхдүүдийн тоо болон ялгаатай нэрний тооны зөрүүг ол.

SELECT (COUNT(*) - COUNT(DISTINCT name)) AS zoruu 
FROM Highschooler

-- 9. Хэрвээ тухайн нэг сурагчид нэгээс олон сурагч дуртай бол уг сурагчийн нэр ангийг харуул.
SELECT name, grade FROM Highschooler h
RIGHT JOIN (
    SELECT COUNT(*) AS Liked, ID2 FROM Likes l
    GROUP BY ID2
    HAVING COUNT(*) > 1
) AS l ON h.ID = l.ID2

-- 10. Хэрвээ сурагч А нь сурагч В-д дуртай, харин Сурагч В нь сургагч С-д дуртай бол эдгээр сурагчдын анги нэрийг харуул.


SELECT h1.name, 
    h1.grade, 
    h2.name, 
    h2.grade, 
    h3.name, 
    h3.grade 
FROM Likes l1, 
    Likes l2, 
    Highschooler h1, 
    Highschooler h2, 
    Highschooler h3
WHERE h1.ID = l1.ID1 AND 
    h2.ID = l1.ID2 AND 
    h3.ID = l2.ID2 AND 
    l1.ID2 = L2.ID1 AND 
    NOT l1.ID1 = L2.ID2


-- 11. Бүх найзууд нь өөр ангид суралцдаг сурагчдын нэр ангийг харуул.


WITH Fs(ID1, ID2, name, grade, aname, agrade) AS (
    SELECT Friend.*, h1.name, h1.grade, h2.name, h2.grade 
    FROM Friend 
    LEFT JOIN Highschooler h1 ON h1.ID = ID1
    LEFT JOIN Highschooler h2 ON h2.ID = ID2
)

SELECT DISTINCT name, grade FROM Fs
WHERE ID1 NOT IN (
    SELECT ID1 FROM Fs 
    WHERE fs.grade = fs.agrade
) AND EXISTS (
    SELECT  ID1 FROM Fs 
    WHERE fs.grade <> fs.agrade
)

-- 12. Оюутануудын найзуудын дундаж хэд вэ? Хариулт нэг тоо гарна.


SELECT CAST(fs AS float) / hs 
FROM (
    SELECT COUNT(*) AS fs 
    FROM Friend
) f, (
    SELECT COUNT(*) AS hs 
    FROM Highschooler
) h



-- 13. Cassandra-н найзууд, мөн түүний найзуудтай найз байх оюутнуудын тоог олно уу. Cassandra -г энэ тоонд оруулахгүй.

WITH CassandraFriends AS (
    SELECT f.ID2 AS ID FROM Friend f 
    LEFT JOIN Highschooler h ON f.ID1= h.ID
    WHERE h.name = 'Cassandra'
),
CassandraID AS (
    SELECT ID FROM Highschooler 
    WHERE name = 'Cassandra'
) 

SELECT cf.naizuud, cff.naizuudiin_naizuud
FROM (
    SELECT COUNT(*) as naizuud 
    FROM CassandraFriends
) cf, (    
    SELECT COUNT(ID2) as naizuudiin_naizuud 
    FROM Friend 
    WHERE ID1 IN(
        SELECT * 
        FROM CassandraFriends
    ) AND ID2 NOT IN(
        SELECT * 
        FROM CassandraID
    ) 
) cff


-- 14. Хамгийн олон найзтай оюутанг олно уу.


WITH FriendCount(ID, naiziin_too) AS (
    SELECT ID1, COUNT(ID2) 
    FROM Friend 
    GROUP BY ID1
)

SELECT h.* FROM FriendCount f
LEFT JOIN Highschooler h ON h.ID = f.ID
WHERE naiziin_too = (
    SELECT MAX(naiziin_too) 
    FROM FriendCount
)


