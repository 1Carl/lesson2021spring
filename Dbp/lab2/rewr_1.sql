-- Өмнө ашигласан кино үнэлгээний (movie rating) өгөгдлийн санд доорх өөрчлөлт хийнэ үү.
-- 1. Roger Ebert нэртэй шинэ шүүмж бичигчийг 209 rID -тайгаар өгөгдлийн сан руу нэмнэ үү.

INSERT INTO Reviewer VALUES(209, 'Roger Ebert')
SELECT * FROM Reviewer

DELETE FROM Reviewer
WHERE name = 'Roger Ebert'
-- 2. James Cameron бүх кинонд 5 үнэлгээ (star) өгөхөөр шийджээ. Түүний шийдвэрийг өгөгдлийн санд оруулна уу. Огноог NULL -р оруулна уу.

INSERT INTO Rating
SELECT re.rID, m.mID, 5, NULL FROM Movie m, Reviewer re
WHERE re.name = 'James Cameron'
SELECT * FROM Rating


-- 3. Дундаж үнэлгээ нь 4 буюу түүнээс дээш байх кино тус бүрийн нээлтээ хийсэн жилийг 25-р нэмэгдүүлнэ үү. 
-- Жич: шинэ мөр үүсгэхгүй, хуучин мөрөө өөрчилнө.

UPDATE Movie 
SET year = year + 25
WHERE mID in (
    SELECT mID FROM Rating
    GROUP BY mID
    HAVING AVG(stars) >= 4
)
SELECT * FROM Movie


select * from Reviewer
SELECT * FROM Rating
-- 4. 1970-с өмнө эсвэл 2000-с хойш бүтээгдсэн бөгөөд 4-с бага үнэлгээ авч байсан бүх киног устгана уу.


DELETE FROM Movie
WHERE mID in (
    SELECT DISTINCT mID FROM Rating
    WHERE stars < 4
) AND (year < 1970 OR year > 2000)
SELECT * FROM Movie


-- Өмнө ашигласан social өгөгдлийн санд доорх өөрчлөлт хийнэ үү.
