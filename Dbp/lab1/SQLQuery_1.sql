--1
select title from Movie​
where director = 'Steven Spielberg';

--2
select distinct m.year from Movie m​
right join Rating r on m.mID = r.mID​
where r.stars >=4​
order by m.year;

--3
select m.title from Movie m​
left join Rating r on m.mID = r.mID ​
where r.mID is NULL;

--4
select DISTINCT m.title, re.name from Rating ra 
full join Rating ri on ra.mID = ri.mID and ra.rID = ri.rID 
full join Movie m on ra.mID = m.mID 
full join Reviewer re on ra.rID = re.rID
where ra.stars > ri.stars and ra.ratingDate > ri.ratingDate
